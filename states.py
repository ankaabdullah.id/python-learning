import pandas as pd

states = ["California", "Texas", "Florida", "New York"]
population = [10000, 20000, 30000, 35000]

dict_states = {'States': states, 'Population': population}

df_states = pd.DataFrame.from_dict(dict_states)
df_states.to_csv('states.csv', index=False)


# for state in states:
#     if state == "Florida":
#         print(state)

with open('test.txt', 'w') as file:
    file.write("Data successfully scrapped ")